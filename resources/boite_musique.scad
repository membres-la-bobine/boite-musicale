include <arduino.scad>

// box
/*difference(){
    cylinder(h=100, r=100, center=false, $fn=8);
    translate([0,0,1]) cylinder(h=100, r=90, center=false, $fn=8);
    
    // holes for ultrasound
    for (i=[1:7]) {
        rotate([90,0,22.5+45*i]) {
            translate([13,50,-120])
            cylinder(h=50, r=8, center=false, $fn=100);
            translate([-13,50,-120])
            cylinder(h=50, r=8, center=false, $fn=100);
        }
    }
    
    // holes for jack
    for (i=[1:7]) {
        rotate([90,0,22.5+45*i]) {
            translate([0,20,-120])
            cylinder(h=50, r=6.68, center=false, $fn=100);
        }
    }
}*/

// lid
translate([0,0,0])
difference() {
    cylinder(h=30, r1=100, r2=50, center=false, $fn=8);
    translate([0,0,-0.1])
    cylinder(h=28, r1=96, r2=46, center=false, $fn=8);
}
difference() {
cylinder(h=28, r1=46, r2=46, center=false, $fn=8);
cylinder(h=28, r1=42, r2=42, center=false, $fn=8);
}

// separation between sectors
for (i=[1:8]) {
    rotate([90,0,45*i]) {
        translate([0,0,42])
        rotate(a=[0,-90,0])
        linear_extrude(height = 4, center = true, convexity = 10, twist = 0)
        polygon(points=[[0,0],[55,0],[0,30]], paths=[[0,1,2]]); 
    }
}

// led support
for (i=[1:8]) {
    rotate([90,0,22.5+45*i]) {
        translate([0,0,42])
        rotate(a=[0,-90,0])
        linear_extrude(height = 4, center = true, convexity = 10, twist = 0)
        polygon(points=[[0,0],[50,0],[47,2],[0,2]], paths=[[0,1,2,3]]);
    }
}

//translate([-25,-50,0.9])
//bumper(UNO, true);

// jack femelle M8 > 6.68mm: https://fr.rs-online.com/web/p/connecteurs-jack/9075680/



