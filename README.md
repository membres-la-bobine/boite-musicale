# Boite Musicale

Boite octogonale musicale équipée de 7 capteurs à ultrasons qui émet pour chaque capteur une note. Permet de créer des jeux du type 'Simon'

== Matériel ==
* [carte micropython PYB405](https://shop.mchobby.be/fr/micropython/1653-hat-micropython-pyb405-nadhat-3232100016538-garatronic.html)
* 7 x capteurs à ultrasons HC-SR05
* un buzzer 
* quelques LEDs