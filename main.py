# musical box by la bobine fablab

from pyb import LED
from ultrasonic import Ultrasonic
import time, math
from pyb import Pin, Timer

US_SENSOR_NUMBER = 7;

notes = { 0 : 261, # Do
          1 : 294, # Ré 
          2 : 329, # Mi
          3 : 349, # Fa
          4 : 392, # Sol
          5 : 440, # La
          6 : 493, # Si 
        }

# init ultra sound sensors
us_sensor = []
us_sensor.append(Ultrasonic(trigger_pin='Y6', echo_pin='X8', timeout_us=30000))
us_sensor.append(Ultrasonic(trigger_pin='Y7', echo_pin='X12', timeout_us=30000))
us_sensor.append(Ultrasonic(trigger_pin='Y8', echo_pin='X6'))
us_sensor.append(Ultrasonic(trigger_pin='Y9', echo_pin='X1'))
us_sensor.append(Ultrasonic(trigger_pin='Y5', echo_pin='X7'))
us_sensor.append(Ultrasonic(trigger_pin='X9', echo_pin='X3'))
us_sensor.append(Ultrasonic(trigger_pin='X10', echo_pin='X5'))


# Configurer les broches PWM pour la sortie sur buzzer
frequence = 0.0
p2 = Pin("Y3") # Broche Y2 avec timer 8 et Channel 2
tim = Timer(4, freq=3000)
ch = tim.channel(3, Timer.PWM, pin=p2)
ch.pulse_width_percent(0)

time_without_detection = 0
while 1:
    detected = False
    for i in range(0,US_SENSOR_NUMBER):
        distance = us_sensor[i].distance_in_cm()
        if distance <= 5 and distance > 0:
            print('Detected on: ', i, ' distance: ', distance)
            tim.freq(notes[i])
           # tim.freq(int(abs((distance)*100)))
            ch.pulse_width_percent(50)
            detected = True
            break
    if not detected:
        time_without_detection = time_without_detection + 1
    if time_without_detection >= 10:
        time_without_detection = 0
        ch.pulse_width_percent(0)
    time.sleep_ms(10)
